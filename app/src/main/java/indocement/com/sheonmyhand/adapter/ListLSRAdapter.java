package indocement.com.sheonmyhand.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import indocement.com.sheonmyhand.model.LsrHandbook;

/**
 * Created by Ridho on 3/31/2017.
 */

public class ListLSRAdapter extends BaseAdapter {

    ArrayList<LsrHandbook> lsrHandbooks;

    public ListLSRAdapter(ArrayList<LsrHandbook> lsrHandbooks) {
        this.lsrHandbooks = lsrHandbooks;
    }

    @Override
    public int getCount() {
        return lsrHandbooks.size();
    }

    @Override
    public Object getItem(int position) {
        return lsrHandbooks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return null;
    }
}
