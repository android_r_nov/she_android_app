package indocement.com.sheonmyhand.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.google.android.gms.phenotype.Flag;

import java.util.ArrayList;

import indocement.com.sheonmyhand.R;
import indocement.com.sheonmyhand.SheDetailActivity;
import indocement.com.sheonmyhand.model.LsrHandbook;
import indocement.com.sheonmyhand.model.SheInfo;

/**
 * Created by Ridho on 3/31/2017.
 */

public class ListSheInfoAdapter extends BaseAdapter {

    ArrayList<SheInfo> sheInfos;
    Context context;

    public ListSheInfoAdapter(Context context, ArrayList<SheInfo> sheInfos) {
        this.sheInfos = sheInfos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return sheInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return sheInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(context);
            v = vi.inflate(R.layout.item_she_info, null);
        }

        final SheInfo p = (SheInfo) getItem(position);

        if (p != null) {
            Button button = (Button) v.findViewById(R.id.button2);
            if (button != null) {
                button.setText(p.getTitle());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, SheDetailActivity.class);
                        intent.putExtra("url", p.getUrl());
                        intent.putExtra("judul", p.getTitle());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
            }

        }

        return v;
    }

}
