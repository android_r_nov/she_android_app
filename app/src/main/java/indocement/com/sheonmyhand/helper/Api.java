package indocement.com.sheonmyhand.helper;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

import indocement.com.sheonmyhand.model.AlatPelindungDiri;
import indocement.com.sheonmyhand.model.HouseKeeping;
import indocement.com.sheonmyhand.model.Isop;
import indocement.com.sheonmyhand.model.MHR;
import indocement.com.sheonmyhand.model.PelaratanDanPerlindungan;
import indocement.com.sheonmyhand.model.Posisi;
import indocement.com.sheonmyhand.model.Reaksi;
import indocement.com.sheonmyhand.model.Sop;

/**
 * Created by Ridho on 12/17/2016.
 */
public class Api {

    //        String host = "http://10.0.2.2/she";
    String host = "http://sheindocement.webmurah-bogor.com";
    //        String host = "http://sheindocement.com";
    String path = "/api.php?";
    private ConnectionHelper helper;

    public Api() {
        this.helper = new ConnectionHelper();
    }

    public String login(Object username, Object password) {
        String url = host + path + "operasi=login&username=" + username + "&password=" + password;
        Log.d("Api", "url : " + url);
        return helper.sendRequest(url);
    }

    public String saveMHR(MHR mhr) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
        String url = null;
        try {
            url = host + path + "operasi=input_mhr" +
                    "&tgl=" + sdf1.format(mhr.getTgl()) +
                    "&audit_team=" + URLEncoder.encode(mhr.getAuditTeam(), "utf-8") +
                    "&area=" + URLEncoder.encode(mhr.getArea(), "utf-8") +
                    "&total_observed=" + URLEncoder.encode(mhr.getTotalObserved(), "utf-8") +
                    "&job_description=" + URLEncoder.encode(mhr.getJobDescription(), "utf-8") +
                    "&responsible1=" + URLEncoder.encode(mhr.getResponsible1(), "utf-8") +
                    "&responsible2=" + URLEncoder.encode(mhr.getResponsible2(), "utf-8") +
                    "&total_people=" + URLEncoder.encode(mhr.getTotalPeople(), "utf-8") +
                    "&people1=" + URLEncoder.encode(mhr.getPeople1(), "utf-8") +
                    "&people2=" + URLEncoder.encode(mhr.getPeople2(), "utf-8") +
                    "&violations1=" + URLEncoder.encode(mhr.getViolations1(), "utf-8") +
                    "&violations2=" + URLEncoder.encode(mhr.getViolations2(), "utf-8") +
                    "&tgl_input=" + sdf1.format(mhr.getTglInput()) +
                    "&nik_penginput=" + mhr.getNikPenginput() +
                    "&jam=" + URLEncoder.encode(mhr.getJam(), "utf-8");


        } catch (Exception e) {

        }
        Log.d("Api", "url : " + url);

        return url;
    }

    public String saveIsop(Isop isop) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        String url = host + path + "operasi=input_isop_card";

        AlatPelindungDiri alatPelindungDiri = isop.getAlatPelindungDiri();
        String alatPerlindunganDiriString = "";
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "kepala_bagian_atas=" + (alatPelindungDiri.isKepala_bagian_atas() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "muka=" + (alatPelindungDiri.isMuka() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "mata=" + (alatPelindungDiri.isMata() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "telinga=" + (alatPelindungDiri.isTelinga() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "hidung=" + (alatPelindungDiri.isHidung() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "tangan_lengan=" + (alatPelindungDiri.isTangan_lengan() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "tubuh=" + (alatPelindungDiri.isTubuh() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "kaki_telapak_kaki=" + (alatPelindungDiri.isKaki_telapak_kaki() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "lain_lain1=" + (alatPelindungDiri.isLain_lain() ? "Y" : "T");
        alatPerlindunganDiriString = alatPerlindunganDiriString + "&" + "isi_lain_lain1=" + (alatPelindungDiri.getIsi_lain_lain());

        url = url + alatPerlindunganDiriString;

        HouseKeeping houseKeeping = isop.getHouseKeeping();
        String houseKeepingString = "";
        houseKeepingString = houseKeepingString + "&" + "pemilahan=" + (houseKeeping.isPemilahan() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "penataan=" + (houseKeeping.isPenataan() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "pembersihan=" + (houseKeeping.isPembersihan() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "perawatan=" + (houseKeeping.isPerawatan() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "pengendalian=" + (houseKeeping.isPengendalian() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "lain_lain2=" + (houseKeeping.isLain_lain() ? "Y" : "T");
        houseKeepingString = houseKeepingString + "&" + "isi_lain_lain2=" + (houseKeeping.getIsi_lain_lain());
        url = url + houseKeepingString;

        PelaratanDanPerlindungan pdp = isop.getPelaratanDanPerlindungan();
        String pspString = "";
        pspString = pspString + "&" + "tidak_sesuai_standar=" + (pdp.isTidak_sesuai_standar() ? "Y" : "T");
        pspString = pspString + "&" + "tidak_dalam_kondisi_aman=" + (pdp.isTidak_dalam_kondisi_aman() ? "Y" : "T");
        pspString = pspString + "&" + "tidak_digunakan_dengan_benar=" + (pdp.isTidak_digunakan_dengan_benar() ? "Y" : "T");
        pspString = pspString + "&" + "lain_lain3=" + (pdp.isLain_lain() ? "Y" : "T");
        pspString = pspString + "&" + "isi_lain_lain3=" + (pdp.getIsi_lain_lain());

        url = url + pspString;

        Posisi posisi = isop.getPosisi();
        String posisiString = "";
        posisiString = posisiString + "&" + "terjatuh=" + (posisi.isTerjatuh() ? "Y" : "T");
        posisiString = posisiString + "&" + "terjepit=" + (posisi.isTerjepit() ? "Y" : "T");
        posisiString = posisiString + "&" + "membentur=" + (posisi.isMembentur() ? "Y" : "T");
        posisiString = posisiString + "&" + "kontak_energi_panas=" + (posisi.isKontak_energi_panas() ? "Y" : "T");
        posisiString = posisiString + "&" + "menghirup=" + (posisi.isMenghirup() ? "Y" : "T");
        posisiString = posisiString + "&" + "faktor_ergonomi=" + (posisi.isFaktor_ergonomi() ? "Y" : "T");
        posisiString = posisiString + "&" + "terparan_terpapar=" + (posisi.isTerparan_terpapar() ? "Y" : "T");
        posisiString = posisiString + "&" + "lain_lain4=" + (posisi.isLain_lain() ? "Y" : "T");
        posisiString = posisiString + "&" + "isi_lain_lain4=" + (posisi.getIsi_lain_lain());
        url = url + posisiString;

        Reaksi reaksi = isop.getReaksi();
        String reaksiString = "";
        reaksiString = reaksiString + "&" + "merubah_posisi=" + (reaksi.isMerubah_posisi() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "menghentikan_pekerjaan=" + (reaksi.isMenghentikan_pekerjaan() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "memakai=" + (reaksi.isMemakai() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "memasang_look_out=" + (reaksi.isMemasang_look_out() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "membuang_pengaman=" + (reaksi.isMembuang_pengaman() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "lain_lain5=" + (reaksi.isLain_lain() ? "Y" : "T");
        reaksiString = reaksiString + "&" + "isi_lain_lain5=" + (reaksi.getIsi_lain_lain());
        url = url + reaksiString;

        Sop sop = isop.getSop();
        String sopString = "";
        sopString = sopString + "&" + "tidak_tersedia=" + (sop.isTidak_tersedia() ? "Y" : "T");
        sopString = sopString + "&" + "tidak_mencukupi=" + (sop.isTidak_mencukupi() ? "Y" : "T");
        sopString = sopString + "&" + "tidak_diketahui=" + (sop.isTidak_diketahui() ? "Y" : "T");
        sopString = sopString + "&" + "tidak_diterapkan=" + (sop.isTidak_diterapkan() ? "Y" : "T");
//        sopString = sopString + "&" + "membuang_pengaman=" + (sop.isMembuang_pengaman() ? "Y" : "T");
        sopString = sopString + "&" + "lain_lain6=" + (sop.isLain_lain() ? "Y" : "T");
        sopString = sopString + "&" + "isi_lain_lain6=" + (sop.getIsi_lain_lain());

        //=====
        try {
            url = url + "&alat_perlindungan_diri=" + (isop.getIsAlatPelingdungDiri() ? "Y" : "T");
            url = url + "&house_keeping=" + (isop.getIsHouseKeeping() ? "Y" : "T");
            url = url + "&peralatan_dan_perlindungan=" + (isop.getIsPeralatanDanPerlindungan() ? "Y" : "T");
            url = url + "&posisi=" + (isop.getIsPosisi() ? "Y" : "T");
            url = url + "&reaksi=" + (isop.getIsReaksi() ? "Y" : "T");
            url = url + "&sop=" + (isop.getIsSOP() ? "Y" : "T");

            url = url + "&tgl_input=" + URLEncoder.encode(sdf1.format(isop.getTgl_input()), "utf-8");
            url = url + "&nik_penginput=" + URLEncoder.encode(isop.getNikPenginput(), "utf-8");
            url = url + "&unit=" + URLEncoder.encode(isop.getUnit(), "utf-8");
            url = url + "&plant=" + URLEncoder.encode(isop.getPlant(), "utf-8");
            url = url + "&nik_optional=" + URLEncoder.encode(isop.getNikOptional(), "utf-8");
            url = url + "&activity=" + URLEncoder.encode(isop.getActivity(), "utf-8");
            url = url + "&departement=" + URLEncoder.encode(isop.getDepartement(), "utf-8");

            url = url + "&date=" + URLEncoder.encode(sdf2.format(isop.getDate()), "utf-8");
            url = url + "&jam=" + URLEncoder.encode(isop.getJam(), "utf-8");

            url = url + "&comment=" + URLEncoder.encode(isop.getComment(), "utf-8");
            url = url + "&recomendation=" + URLEncoder.encode(isop.getRecommendation(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        url = url + sopString;


        Log.d("URL", url);

        return helper.sendRequest(url);
    }

    public String getLsrHandbooks() {
        String url = host + path + "operasi=get_available_lsr_handbook";
        Log.d("Api", "url : " + url);
        return url;
    }

    public String getLsrHandbookUrl() {
        String url = host + path + "operasi=view_lsr_handbook&id_lsr=01";
        Log.d("Api", "url : " + url);
        return helper.sendRequest(url);
    }


    public String getAvailableSheInfo() {
//        http://sheindocement.webmurah-bogor.com/api.php?operasi=view_she_info
        String url = host + path + "operasi=view_she_info";
        Log.d("Api", "url : " + url);
        return helper.sendRequest(url);
    }

    public String getSheINfoDetail(String idInfo) {
        String url = host + path + "operasi=view_detail_she_info&id_info=" + idInfo;
        Log.d("Api", "url : " + url);
        return helper.sendRequest(url);
    }

    public String forgetPasssword(String email) {
        String url = host + path + "operasi=lupa_password&email=" + email;
        Log.d("Api", "url : " + url);
        return helper.sendRequest(url);
    }
}
