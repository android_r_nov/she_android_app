package indocement.com.sheonmyhand.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Ridho on 12/17/2016.
 */
public class ConnectionHelper {

    public String sendRequest(String urlString) {
        String server_response = "";

        URL url;
//        HttpURLConnection urlConnection = null;
//        try {
//            Log.d("REQUEST", "URL :" + urlString);
//            url = new URL(urlString);
//            urlConnection = (HttpURLConnection) url
//                    .openConnection();
//
//            int responseCode = urlConnection.getResponseCode();
//
//            if (responseCode == HttpURLConnection.HTTP_OK) {
//                server_response = readStream(urlConnection.getInputStream());
//                Log.v("CatalogClient", server_response);
//            }
//            return server_response;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "Error : " + e.getMessage();
//        } finally {
//            if (urlConnection != null) {
//                urlConnection.disconnect();
//            }
//        }
        try {
            url = new URL(urlString);
            int respCode = 0;

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            //httpURLConnection = (HttpURLConnection) url.openConnection();
            //  HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());

            httpURLConnection.setRequestMethod("GET");

            httpURLConnection.setRequestProperty("Connection", "keep-alive");
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/json");
            httpURLConnection.setRequestProperty("UseCookieContainer", "True");
            httpURLConnection.setChunkedStreamingMode(0);
            httpURLConnection.connect();

            if (httpURLConnection != null) {
                respCode = httpURLConnection.getResponseCode();
//                messageStatus.setResponseCode(respCode);
            }
            if (respCode == HttpURLConnection.HTTP_OK) {
                InputStream responseStream = httpURLConnection.getInputStream();
                server_response = readStream(responseStream);
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return server_response;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

}
