package indocement.com.sheonmyhand;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import indocement.com.sheonmyhand.helper.Api;
import indocement.com.sheonmyhand.helper.Helper;
import indocement.com.sheonmyhand.model.AlatPelindungDiri;
import indocement.com.sheonmyhand.model.HouseKeeping;
import indocement.com.sheonmyhand.model.Isop;
import indocement.com.sheonmyhand.model.MHR;
import indocement.com.sheonmyhand.model.PelaratanDanPerlindungan;
import indocement.com.sheonmyhand.model.Posisi;
import indocement.com.sheonmyhand.model.Reaksi;
import indocement.com.sheonmyhand.model.Sop;
import indocement.com.sheonmyhand.model.User;

public class IsopCardActivity extends AppCompatActivity implements View.OnClickListener {


    EditText editTextObserverId, editTextPlant, editTextDepartement, editTextNikObserved, editTextActivity, editTextComment, editTextRecommendation;
    Spinner spinnerUnit;
    TextView textViewObserverName;
    DatePicker datePickerObservationDate;
    TimePicker timePickerObservationTime;
    private String format = "";

    CheckBox checkBoxAlatPelingdungDiri, checkBoxReaksi, checkBoxHouseKeeping, checkBoxPosisi, checkBoPeralatanDanPerlindungan, checkBoxSOP;

    CheckBox checkBoxKepalaBagianAtas, checkBoxMuka,
            checkBoxMata, checkBoxTelinga, checkBoxHidung,
            checkBoxTanganDanLengan, checkBoxTubuh, checkBoxKaki, checkBoxLainPerlindunganDiri;


    CheckBox checkBoxPemilahan, checkBoxPenataan,
            checkBoxPembersihan, checkBoxPerawatan, checkBoxPengendalian,
            checkBoxLainHouseKeeping;

    CheckBox checkBoxTidakSesuaiStandar, checkBoxTidakDalamKondisiAman,
            checkBoxTidakDigunakanDenganBenar, checkBoxLainPelengkapanDanPellindungan;

    CheckBox checkBoxTerjatuh, checkBoxTerjepit, checkBoxMembentur, checkBoxKontakEnergiPanas,
            checkBoxMenghirup, checkBoxFaktorergonomi, checkBoxTerpaparPaparan,
            checkBoxLainPosisi;

    CheckBox checkBoxMerubahPosisi, checkBoxMenhentikanPekarjaan, checkBoxMemakai, checkBoxMemasangLockOut,
            checkBoxMembuangPengaman, checkBoxLainReaksi;

    CheckBox checkBoxTidakTersedia, checkBoxTidakMencukupi, checkBoxTidakDiketahui, checkBoxTidakDiterapkan,
            checkBoxLainSop;

    Button buttonSimpanIsopCard;
    Api api = new Api();
    AlertDialog dialog;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isop_card);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hand_logo);
        setupViews();
        Bundle b;
        if (getIntent().getExtras() != null) {
            b = getIntent().getExtras();
            user = (User) b.getSerializable("user");
            editTextObserverId.setText(user.getNik());
            textViewObserverName.setText("Name : " + user.getNama());
        }

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.unit_location, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerUnit.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.menu_home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setupViews() {

        editTextObserverId = (EditText) findViewById(R.id.editTextObserverId);
        editTextPlant = (EditText) findViewById(R.id.editTextPlant);
        editTextDepartement = (EditText) findViewById(R.id.editTextDepartement);
        editTextNikObserved = (EditText) findViewById(R.id.editTextNikObserved);
        editTextActivity = (EditText) findViewById(R.id.editTextActivity);
        editTextComment = (EditText) findViewById(R.id.editTextComment);
        editTextRecommendation = (EditText) findViewById(R.id.editTextRecommendation);

        textViewObserverName = (TextView) findViewById(R.id.textViewObserverName);

        spinnerUnit = (Spinner) findViewById(R.id.spinnerUnit);
        datePickerObservationDate = (DatePicker) findViewById(R.id.datePickerObservationDate);
        timePickerObservationTime = (TimePicker) findViewById(R.id.timePickerObservationTime);

        //===========
        checkBoxAlatPelingdungDiri = (CheckBox) findViewById(R.id.checkBoxAlatPelingdungDiri);
        checkBoxReaksi = (CheckBox) findViewById(R.id.checkBoxReaksi);
        checkBoxHouseKeeping = (CheckBox) findViewById(R.id.checkBoxHouseKeeping);
        checkBoxPosisi = (CheckBox) findViewById(R.id.checkBoxPosisi);
        checkBoPeralatanDanPerlindungan = (CheckBox) findViewById(R.id.checkBoPeralatanDanPerlindungan);
        checkBoxSOP = (CheckBox) findViewById(R.id.checkBoxSOP);
        //=============

        checkBoxKepalaBagianAtas = (CheckBox) findViewById(R.id.checkBoxKepalaBagianAtas);
        checkBoxMuka = (CheckBox) findViewById(R.id.checkBoxMuka);
        checkBoxMata = (CheckBox) findViewById(R.id.checkBoxMata);
        checkBoxTelinga = (CheckBox) findViewById(R.id.checkBoxTelinga);
        checkBoxHidung = (CheckBox) findViewById(R.id.checkBoxHidung);
        checkBoxTanganDanLengan = (CheckBox) findViewById(R.id.checkBoxTanganDanLengan);
        checkBoxTubuh = (CheckBox) findViewById(R.id.checkBoxTubuh);
        checkBoxKaki = (CheckBox) findViewById(R.id.checkBoxKakiDanTelapakKaki);
        checkBoxLainPerlindunganDiri = (CheckBox) findViewById(R.id.checkBoxLainLainPelindungDiri);


        checkBoxPemilahan = (CheckBox) findViewById(R.id.checkBoxPemilahan);
        checkBoxPenataan = (CheckBox) findViewById(R.id.checkBoxPenataan);
        checkBoxPembersihan = (CheckBox) findViewById(R.id.checkBoxPembersihan);
        checkBoxPerawatan = (CheckBox) findViewById(R.id.checkBoxPerawatan);
        checkBoxPengendalian = (CheckBox) findViewById(R.id.checkBoxPengendalian);
        checkBoxLainHouseKeeping = (CheckBox) findViewById(R.id.checkBoxLainHouseKeep);

        checkBoxTidakSesuaiStandar = (CheckBox) findViewById(R.id.checkBoxTidakSesuaiStandar);
        checkBoxTidakDalamKondisiAman = (CheckBox) findViewById(R.id.checkBoxTidakKondisiAman);
        checkBoxTidakDigunakanDenganBenar = (CheckBox) findViewById(R.id.checkBoxTidakDigunakanDgAman);
        checkBoxLainPelengkapanDanPellindungan = (CheckBox) findViewById(R.id.checkBoxLainPeralatanPerlindungan);


        checkBoxTerjatuh = (CheckBox) findViewById(R.id.checkBoxTerjatuh);
        checkBoxTerjepit = (CheckBox) findViewById(R.id.checkBoxTerjepit);
        checkBoxMembentur = (CheckBox) findViewById(R.id.checkBoxMembentur);
        checkBoxKontakEnergiPanas = (CheckBox) findViewById(R.id.checkBoxKontakPanas);
        checkBoxMenghirup = (CheckBox) findViewById(R.id.checkBoxMenghirup);
        checkBoxFaktorergonomi = (CheckBox) findViewById(R.id.checkBoxErgonomi);
        checkBoxTerpaparPaparan = (CheckBox) findViewById(R.id.checkBoxTerpapar);
        checkBoxLainPosisi = (CheckBox) findViewById(R.id.checkBoxLainPosisi);

        checkBoxMerubahPosisi = (CheckBox) findViewById(R.id.checkBoxMerubahPosisi);
        checkBoxMenhentikanPekarjaan = (CheckBox) findViewById(R.id.checkBoxMenghentikanPekerjaan);
        checkBoxMemakai = (CheckBox) findViewById(R.id.checkBoxMemakaiAPD);
        checkBoxMemasangLockOut = (CheckBox) findViewById(R.id.checkBoxLOTO);
        checkBoxMembuangPengaman = (CheckBox) findViewById(R.id.checkBoxMemasangPengaman);
        checkBoxLainReaksi = (CheckBox) findViewById(R.id.checkBoxLainReaksi);

        checkBoxTidakTersedia = (CheckBox) findViewById(R.id.checkBoxTidakTersedia);
        checkBoxTidakMencukupi = (CheckBox) findViewById(R.id.checkBoxTidakMendukupiPotensi);
        checkBoxTidakDiketahui = (CheckBox) findViewById(R.id.checkBoxTidakDiketahui);
        checkBoxTidakDiterapkan = (CheckBox) findViewById(R.id.checkBoxTidakDiterapkan);
        checkBoxLainSop = (CheckBox) findViewById(R.id.checkBoxLainSOP);
        buttonSimpanIsopCard = (Button) findViewById(R.id.buttonSimpanIsopCard);
        buttonSimpanIsopCard.setOnClickListener(this);
    }


    public static Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }


    private Isop generateIsop() {
        Isop isop = new Isop();

        //=====
        isop.setIsHouseKeeping(checkBoxHouseKeeping.isChecked());
        isop.setIsPeralatanDanPerlindungan(checkBoPeralatanDanPerlindungan.isChecked());
        isop.setIsPosisi(checkBoxPosisi.isChecked());
        isop.setIsReaksi(checkBoxReaksi.isChecked());
        isop.setIsSOP(checkBoxSOP.isChecked());
        isop.setIsAlatPelingdungDiri(checkBoxAlatPelingdungDiri.isChecked());

        //=====

        String plant = editTextPlant.getText().toString();
        String departement = editTextDepartement.getText().toString();
        String activity = editTextActivity.getText().toString();
        String comment = editTextComment.getText().toString();
        String recommendation = editTextRecommendation.getText().toString();

        if (plant.equals("") || plant == null)
            return null;
        if (departement.equals("") || departement == null)
            return null;
        if (activity.equals("") || activity == null)
            return null;
        if (comment.equals("") || comment == null)
            return null;
        if (recommendation.equals("") || recommendation == null)
            return null;


        String unit = spinnerUnit.getSelectedItem().toString();
        isop.setUnit(unit);
        isop.setPlant(plant);
        isop.setDepartement(departement);
        isop.setNikOptional(editTextNikObserved.getText().toString());
        isop.setActivity(activity);
        isop.setComment(comment);
        isop.setRecommendation(recommendation);

        isop.setDate(getDateFromDatePicker(datePickerObservationDate));
        isop.setJam(showTime(timePickerObservationTime.getCurrentHour(), timePickerObservationTime.getCurrentMinute()));

        isop.setAlatPelindungDiri(generateAlatPerlindunganDiri());
        isop.setHouseKeeping(generateHouseKeeping());
        isop.setPelaratanDanPerlindungan(generatePeralatanDanPerlindungan());
        isop.setPosisi(generatePosisi());
        isop.setReaksi(generateReaksi());
        isop.setSop(generateSOP());
        isop.setTgl_input(new Date());
        isop.setNikPenginput(user.getNik());


        return isop;
    }


    public String showTime(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        StringBuilder strBuilder = new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format);
        return strBuilder.toString();
    }

    private AlatPelindungDiri generateAlatPerlindunganDiri() {
        AlatPelindungDiri alatPelindungDiri = new AlatPelindungDiri();
        alatPelindungDiri.setKepala_bagian_atas(checkBoxKepalaBagianAtas.isChecked());
        alatPelindungDiri.setMuka(checkBoxTanganDanLengan.isChecked());
        alatPelindungDiri.setMata(checkBoxMata.isChecked());
        alatPelindungDiri.setTelinga(checkBoxTelinga.isChecked());
        alatPelindungDiri.setHidung(checkBoxHidung.isChecked());
        alatPelindungDiri.setTangan_lengan(checkBoxTanganDanLengan.isChecked());
        alatPelindungDiri.setTubuh(checkBoxTubuh.isChecked());
        alatPelindungDiri.setKaki_telapak_kaki(checkBoxKaki.isChecked());
        alatPelindungDiri.setLain_lain(checkBoxLainPerlindunganDiri.isChecked());
        return alatPelindungDiri;
    }

    private HouseKeeping generateHouseKeeping() {
        HouseKeeping houseKeeping = new HouseKeeping();
        houseKeeping.setPemilahan(checkBoxPemilahan.isChecked());
        houseKeeping.setPenataan(checkBoxPenataan.isChecked());
        houseKeeping.setPembersihan(checkBoxPembersihan.isChecked());
        houseKeeping.setPerawatan(checkBoxPerawatan.isChecked());
        houseKeeping.setPengendalian(checkBoxPengendalian.isChecked());
        houseKeeping.setLain_lain(checkBoxLainHouseKeeping.isChecked());
        return houseKeeping;
    }

    private PelaratanDanPerlindungan generatePeralatanDanPerlindungan() {
        PelaratanDanPerlindungan pelaratanDanPerlindungan = new PelaratanDanPerlindungan();
        pelaratanDanPerlindungan.setTidak_sesuai_standar(checkBoxTidakSesuaiStandar.isChecked());
        pelaratanDanPerlindungan.setTidak_dalam_kondisi_aman(checkBoxTidakDalamKondisiAman.isChecked());
        pelaratanDanPerlindungan.setTidak_digunakan_dengan_benar(checkBoxTidakDigunakanDenganBenar.isChecked());
        pelaratanDanPerlindungan.setLain_lain(checkBoxLainPelengkapanDanPellindungan.isChecked());
        return pelaratanDanPerlindungan;
    }

    private Posisi generatePosisi() {
        Posisi posisi = new Posisi();

        posisi.setTerjatuh(checkBoxTerjatuh.isChecked());
        posisi.setTerjepit(checkBoxTerjepit.isChecked());
        posisi.setMembentur(checkBoxMembentur.isChecked());
        posisi.setMenghirup(checkBoxMenghirup.isChecked());
        posisi.setFaktor_ergonomi(checkBoxFaktorergonomi.isChecked());
        posisi.setTerparan_terpapar(checkBoxTerpaparPaparan.isChecked());
        posisi.setKontak_energi_panas(checkBoxKontakEnergiPanas.isChecked());
        posisi.setLain_lain(checkBoxLainPosisi.isChecked());
        return posisi;
    }

    private Reaksi generateReaksi() {
        Reaksi reaksi = new Reaksi();

        reaksi.setMerubah_posisi(checkBoxMerubahPosisi.isChecked());
        reaksi.setMenghentikan_pekerjaan(checkBoxMenhentikanPekarjaan.isChecked());
        reaksi.setMemakai(checkBoxMemakai.isChecked());
        reaksi.setMemasang_look_out(checkBoxMemasangLockOut.isChecked());
        reaksi.setMembuang_pengaman(checkBoxMembuangPengaman.isChecked());
        reaksi.setLain_lain(checkBoxLainReaksi.isChecked());
        return reaksi;
    }

    private Sop generateSOP() {
        Sop sop = new Sop();

        sop.setTidak_tersedia(checkBoxTidakTersedia.isChecked());
        sop.setTidak_mencukupi(checkBoxTidakMencukupi.isChecked());
        sop.setTidak_diketahui(checkBoxTidakDiketahui.isChecked());
        sop.setTidak_diterapkan(checkBoxTidakDiterapkan.isChecked());
        sop.setLain_lain(checkBoxLainSop.isChecked());
        return sop;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSimpanIsopCard:
                simpan(generateIsop());
                break;
            default:
                break;
        }
    }

    public void createDialog(String judul, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }

    private void simpan(final Isop isop) {
//        Helper.hideSoftKeyboard(this);

        if (isop == null) {
            createDialog("Perhatian!", "Harap lengkapi kolom-kolom yang kosong");
            dialog.show();
        } else {
            final ProgressDialog progressDialog = Helper.showLoading(this, "Harap tunggu ...");
            AsyncTask task = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    return api.saveIsop(isop);
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    progressDialog.dismiss();
                    if (o.toString().equals("Data berhasil terkirim")) {
                        createDialog("Berhasil", o.toString());
                        dialog.show();
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                resetViews();
                            }
                        });
                    } else {
                        createDialog("Gagal mengirim data", o.toString());
                        dialog.show();
                    }
                    Log.d("MHR", o.toString());
                }
            };

            task.execute();
        }

    }

    private void resetViews() {

        editTextPlant.setText("");
        editTextDepartement.setText("");
        editTextNikObserved.setText("");
        editTextActivity.setText("");
        editTextComment.setText("");
        editTextRecommendation.setText("");

        checkBoxKepalaBagianAtas.setChecked(false);
        checkBoxMuka.setChecked(false);
        checkBoxMata.setChecked(false);
        checkBoxTelinga.setChecked(false);
        checkBoxHidung.setChecked(false);
        checkBoxTanganDanLengan.setChecked(false);
        checkBoxTubuh.setChecked(false);
        checkBoxKaki.setChecked(false);
        checkBoxLainPerlindunganDiri.setChecked(false);


        checkBoxPemilahan.setChecked(false);
        checkBoxPenataan.setChecked(false);
        checkBoxPembersihan.setChecked(false);
        checkBoxPerawatan.setChecked(false);
        checkBoxPengendalian.setChecked(false);
        checkBoxLainHouseKeeping.setChecked(false);

        checkBoxTidakSesuaiStandar.setChecked(false);
        checkBoxTidakDalamKondisiAman.setChecked(false);
        checkBoxTidakDigunakanDenganBenar.setChecked(false);
        checkBoxLainPelengkapanDanPellindungan.setChecked(false);


        checkBoxTerjatuh.setChecked(false);
        checkBoxTerjepit.setChecked(false);
        checkBoxMembentur.setChecked(false);
        checkBoxKontakEnergiPanas.setChecked(false);
        checkBoxMenghirup.setChecked(false);
        checkBoxFaktorergonomi.setChecked(false);
        checkBoxTerpaparPaparan.setChecked(false);
        checkBoxLainPosisi.setChecked(false);

        checkBoxMerubahPosisi.setChecked(false);
        checkBoxMenhentikanPekarjaan.setChecked(false);
        checkBoxMemakai.setChecked(false);
        checkBoxMemasangLockOut.setChecked(false);
        checkBoxMembuangPengaman.setChecked(false);
        checkBoxLainReaksi.setChecked(false);

        checkBoxTidakTersedia.setChecked(false);
        checkBoxTidakMencukupi.setChecked(false);
        checkBoxTidakDiketahui.setChecked(false);
        checkBoxTidakDiterapkan.setChecked(false);
        checkBoxLainSop.setChecked(false);
    }
}
