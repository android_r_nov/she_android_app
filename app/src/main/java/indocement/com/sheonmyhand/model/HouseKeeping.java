package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class HouseKeeping {

    boolean pemilahan;
    boolean penataan;
    boolean pembersihan;
    boolean perawatan;
    boolean pengendalian;
    boolean lain_lain;
    String isi_lain_lain;

    public boolean isPemilahan() {
        return pemilahan;
    }

    public void setPemilahan(boolean pemilahan) {
        this.pemilahan = pemilahan;
    }

    public boolean isPenataan() {
        return penataan;
    }

    public void setPenataan(boolean penataan) {
        this.penataan = penataan;
    }

    public boolean isPembersihan() {
        return pembersihan;
    }

    public void setPembersihan(boolean pembersihan) {
        this.pembersihan = pembersihan;
    }

    public boolean isPerawatan() {
        return perawatan;
    }

    public void setPerawatan(boolean perawatan) {
        this.perawatan = perawatan;
    }

    public boolean isPengendalian() {
        return pengendalian;
    }

    public void setPengendalian(boolean pengendalian) {
        this.pengendalian = pengendalian;
    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }

    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }
}
