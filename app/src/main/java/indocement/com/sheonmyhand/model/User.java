package indocement.com.sheonmyhand.model;

import java.io.Serializable;

/**
 * Created by Ridho on 3/30/2017.
 */

public class User implements Serializable {

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    String nama;
    String nik;
}
