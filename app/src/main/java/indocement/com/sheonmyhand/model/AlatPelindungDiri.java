package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class AlatPelindungDiri {
    boolean kepala_bagian_atas;
    boolean muka;
    boolean mata;
    boolean telinga;
    boolean hidung;
    boolean tangan_lengan;
    boolean tubuh;
    boolean kaki_telapak_kaki;
    boolean lain_lain;
    String isi_lain_lain;


    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }

    public boolean isKepala_bagian_atas() {
        return kepala_bagian_atas;
    }

    public void setKepala_bagian_atas(boolean kepala_bagian_atas) {
        this.kepala_bagian_atas = kepala_bagian_atas;
    }

    public boolean isMuka() {
        return muka;
    }

    public void setMuka(boolean muka) {
        this.muka = muka;
    }

    public boolean isMata() {
        return mata;
    }

    public void setMata(boolean mata) {
        this.mata = mata;
    }

    public boolean isTelinga() {
        return telinga;
    }

    public void setTelinga(boolean telinga) {
        this.telinga = telinga;
    }

    public boolean isHidung() {
        return hidung;
    }

    public void setHidung(boolean hidung) {
        this.hidung = hidung;
    }

    public boolean isTangan_lengan() {
        return tangan_lengan;
    }

    public void setTangan_lengan(boolean tangan_lengan) {
        this.tangan_lengan = tangan_lengan;
    }

    public boolean isTubuh() {
        return tubuh;
    }

    public void setTubuh(boolean tubuh) {
        this.tubuh = tubuh;
    }

    public boolean isKaki_telapak_kaki() {
        return kaki_telapak_kaki;
    }

    public void setKaki_telapak_kaki(boolean kaki_telapak_kaki) {
        this.kaki_telapak_kaki = kaki_telapak_kaki;
    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }


}
