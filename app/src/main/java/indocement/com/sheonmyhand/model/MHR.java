package indocement.com.sheonmyhand.model;

import java.util.Date;

/**
 * Created by Ridho on 3/30/2017.
 */

public class MHR {
    public Date getTgl() {
        return tgl;
    }

    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }

    public String getAuditTeam() {
        return auditTeam;
    }

    public void setAuditTeam(String auditTeam) {
        this.auditTeam = auditTeam;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTotalObserved() {
        return totalObserved;
    }

    public void setTotalObserved(String totalObserved) {
        this.totalObserved = totalObserved;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getResponsible1() {
        return responsible1;
    }

    public void setResponsible1(String responsible1) {
        this.responsible1 = responsible1;
    }

    public String getResponsible2() {
        return responsible2;
    }

    public void setResponsible2(String responsible2) {
        this.responsible2 = responsible2;
    }

    public String getPeople1() {
        return people1;
    }

    public void setPeople1(String people1) {
        this.people1 = people1;
    }

    public String getPeople2() {
        return people2;
    }

    public void setPeople2(String people2) {
        this.people2 = people2;
    }

    public String getViolations1() {
        return violations1;
    }

    public void setViolations1(String violations1) {
        this.violations1 = violations1;
    }

    public String getViolations2() {
        return violations2;
    }

    public void setViolations2(String violations2) {
        this.violations2 = violations2;
    }

    public Date getTglInput() {
        return tglInput;
    }

    public void setTglInput(Date tglInput) {
        this.tglInput = tglInput;
    }

    public String getNikPenginput() {
        return nikPenginput;
    }

    public void setNikPenginput(String nikPenginput) {
        this.nikPenginput = nikPenginput;
    }

    public Object getLampiran() {
        return lampiran;
    }

    public void setLampiran(Object lampiran) {
        this.lampiran = lampiran;
    }

    Date tgl;
    String auditTeam;
    String area;
    String totalObserved;

    public String getTotalPeople() {
        return totalPeople;
    }

    public void setTotalPeople(String totalPeople) {
        this.totalPeople = totalPeople;
    }

    String totalPeople;
    String jobDescription;
    String responsible1;
    String responsible2;
    String people1;
    String people2;
    String violations1;
    String violations2;
    Date tglInput;
    String nikPenginput;
    Object lampiran;

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    String jam;
}
