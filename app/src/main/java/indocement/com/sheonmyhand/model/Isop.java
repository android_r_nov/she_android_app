package indocement.com.sheonmyhand.model;

import java.util.Date;

/**
 * Created by Ridho on 3/31/2017.
 */

public class Isop {

    Date tgl_input;
    String nikPenginput;

    AlatPelindungDiri alatPelindungDiri;
    HouseKeeping houseKeeping;
    PelaratanDanPerlindungan pelaratanDanPerlindungan;
    Posisi posisi;
    Reaksi reaksi;
    Sop sop;

    //=========
    String unit;
    String plant;
    String departement;
    String nikOptional;
    String activity;
    Date date;
    String comment;
    String recommendation;
    String jam;


    Boolean isAlatPelingdungDiri;
    Boolean isReaksi;
    Boolean isHouseKeeping;
    Boolean isPosisi;
    Boolean isPeralatanDanPerlindungan;
    Boolean isSOP;


    public Boolean getIsAlatPelingdungDiri() {
        return isAlatPelingdungDiri;
    }

    public void setIsAlatPelingdungDiri(Boolean alatPelingdungDiri) {
        isAlatPelingdungDiri = alatPelingdungDiri;
    }

    public void setIsReaksi(Boolean reaksi) {
        isReaksi = reaksi;
    }

    public Boolean getIsReaksi() {
        return isReaksi;
    }

    public void setIsHouseKeeping(Boolean houseKeeping) {
        isHouseKeeping = houseKeeping;
    }

    public Boolean getIsHouseKeeping() {
        return isHouseKeeping;
    }

    public void setIsPosisi(Boolean posisi) {
        isPosisi = posisi;
    }

    public Boolean getIsPosisi() {
        return isPosisi;
    }

    public Boolean getIsPeralatanDanPerlindungan() {
        return isPeralatanDanPerlindungan;
    }

    public void setIsPeralatanDanPerlindungan(Boolean peralatanDanPerlindungan) {
        isPeralatanDanPerlindungan = peralatanDanPerlindungan;
    }

    public Boolean getIsSOP() {
        return isSOP;
    }

    public void setIsSOP(Boolean SOP) {
        isSOP = SOP;
    }


    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getNikOptional() {
        return nikOptional;
    }

    public void setNikOptional(String nikOptional) {
        this.nikOptional = nikOptional;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }


    public Date getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(Date tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getNikPenginput() {
        return nikPenginput;
    }

    public void setNikPenginput(String nikPenginput) {
        this.nikPenginput = nikPenginput;
    }

    public AlatPelindungDiri getAlatPelindungDiri() {
        return alatPelindungDiri;
    }

    public void setAlatPelindungDiri(AlatPelindungDiri alatPelindungDiri) {
        this.alatPelindungDiri = alatPelindungDiri;
    }

    public HouseKeeping getHouseKeeping() {
        return houseKeeping;
    }

    public void setHouseKeeping(HouseKeeping houseKeeping) {
        this.houseKeeping = houseKeeping;
    }

    public PelaratanDanPerlindungan getPelaratanDanPerlindungan() {
        return pelaratanDanPerlindungan;
    }

    public void setPelaratanDanPerlindungan(PelaratanDanPerlindungan pelaratanDanPerlindungan) {
        this.pelaratanDanPerlindungan = pelaratanDanPerlindungan;
    }

    public Posisi getPosisi() {
        return posisi;
    }

    public void setPosisi(Posisi posisi) {
        this.posisi = posisi;
    }

    public Reaksi getReaksi() {
        return reaksi;
    }

    public void setReaksi(Reaksi reaksi) {
        this.reaksi = reaksi;
    }

    public Sop getSop() {
        return sop;
    }

    public void setSop(Sop sop) {
        this.sop = sop;
    }
}
