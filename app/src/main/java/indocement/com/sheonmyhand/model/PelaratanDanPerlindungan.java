package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class PelaratanDanPerlindungan {

    boolean tidak_sesuai_standar;
    boolean tidak_dalam_kondisi_aman;
    boolean tidak_digunakan_dengan_benar;
    boolean lain_lain;
    String isi_lain_lain;

    public boolean isTidak_sesuai_standar() {
        return tidak_sesuai_standar;
    }

    public void setTidak_sesuai_standar(boolean tidak_sesuai_standar) {
        this.tidak_sesuai_standar = tidak_sesuai_standar;
    }

    public boolean isTidak_dalam_kondisi_aman() {
        return tidak_dalam_kondisi_aman;
    }

    public void setTidak_dalam_kondisi_aman(boolean tidak_dalam_kondisi_aman) {
        this.tidak_dalam_kondisi_aman = tidak_dalam_kondisi_aman;
    }

    public boolean isTidak_digunakan_dengan_benar() {
        return tidak_digunakan_dengan_benar;
    }

    public void setTidak_digunakan_dengan_benar(boolean tidak_digunakan_dengan_benar) {
        this.tidak_digunakan_dengan_benar = tidak_digunakan_dengan_benar;
    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }

    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }
}
