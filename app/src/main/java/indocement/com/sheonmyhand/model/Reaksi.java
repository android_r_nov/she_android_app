package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class Reaksi {
    boolean merubah_posisi;
    boolean menghentikan_pekerjaan;
    boolean memakai;
    boolean memasang_look_out;
    boolean membuang_pengaman;
    boolean lain_lain;
    String isi_lain_lain;

    public boolean isMerubah_posisi() {
        return merubah_posisi;
    }

    public void setMerubah_posisi(boolean merubah_posisi) {
        this.merubah_posisi = merubah_posisi;
    }

    public boolean isMenghentikan_pekerjaan() {
        return menghentikan_pekerjaan;
    }

    public void setMenghentikan_pekerjaan(boolean menghentikan_pekerjaan) {
        this.menghentikan_pekerjaan = menghentikan_pekerjaan;
    }

    public boolean isMemakai() {
        return memakai;
    }

    public void setMemakai(boolean memakai) {
        this.memakai = memakai;
    }

    public boolean isMemasang_look_out() {
        return memasang_look_out;
    }

    public void setMemasang_look_out(boolean memasang_look_out) {
        this.memasang_look_out = memasang_look_out;
    }

    public boolean isMembuang_pengaman() {
        return membuang_pengaman;
    }

    public void setMembuang_pengaman(boolean membuang_pengaman) {
        this.membuang_pengaman = membuang_pengaman;
    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }

    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }
}
