package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 4/9/2017.
 */

public class SheInfo {
    String id;
    String title;
    String url;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
