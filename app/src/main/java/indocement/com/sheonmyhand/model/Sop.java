package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class Sop {
    boolean tidak_tersedia;
    boolean tidak_mencukupi;
    boolean tidak_diketahui;
    boolean tidak_diterapkan;
//    boolean membuang_pengaman;
    boolean lain_lain;
    String isi_lain_lain;


    public boolean isTidak_tersedia() {
        return tidak_tersedia;
    }

    public void setTidak_tersedia(boolean tidak_tersedia) {
        this.tidak_tersedia = tidak_tersedia;
    }

    public boolean isTidak_mencukupi() {
        return tidak_mencukupi;
    }

    public void setTidak_mencukupi(boolean tidak_mencukupi) {
        this.tidak_mencukupi = tidak_mencukupi;
    }

    public boolean isTidak_diketahui() {
        return tidak_diketahui;
    }

    public void setTidak_diketahui(boolean tidak_diketahui) {
        this.tidak_diketahui = tidak_diketahui;
    }

    public boolean isTidak_diterapkan() {
        return tidak_diterapkan;
    }

    public void setTidak_diterapkan(boolean tidak_diterapkan) {
        this.tidak_diterapkan = tidak_diterapkan;
    }

//    public boolean isMembuang_pengaman() {
//        return membuang_pengaman;
//    }
//
//    public void setMembuang_pengaman(boolean membuang_pengaman) {
//        this.membuang_pengaman = membuang_pengaman;
//    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }

    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }
}
