package indocement.com.sheonmyhand.model;

/**
 * Created by Ridho on 3/31/2017.
 */

public class Posisi {
    boolean terjatuh;
    boolean terjepit;
    boolean membentur;
    boolean kontak_energi_panas;
    boolean menghirup;
    boolean faktor_ergonomi;
    boolean terparan_terpapar;
    boolean lain_lain;
    String isi_lain_lain;

    public boolean isTerjatuh() {
        return terjatuh;
    }

    public void setTerjatuh(boolean terjatuh) {
        this.terjatuh = terjatuh;
    }

    public boolean isTerjepit() {
        return terjepit;
    }

    public void setTerjepit(boolean terjepit) {
        this.terjepit = terjepit;
    }

    public boolean isMembentur() {
        return membentur;
    }

    public void setMembentur(boolean membentur) {
        this.membentur = membentur;
    }

    public boolean isKontak_energi_panas() {
        return kontak_energi_panas;
    }

    public void setKontak_energi_panas(boolean kontak_energi_panas) {
        this.kontak_energi_panas = kontak_energi_panas;
    }

    public boolean isMenghirup() {
        return menghirup;
    }

    public void setMenghirup(boolean menghirup) {
        this.menghirup = menghirup;
    }

    public boolean isFaktor_ergonomi() {
        return faktor_ergonomi;
    }

    public void setFaktor_ergonomi(boolean faktor_ergonomi) {
        this.faktor_ergonomi = faktor_ergonomi;
    }

    public boolean isTerparan_terpapar() {
        return terparan_terpapar;
    }

    public void setTerparan_terpapar(boolean terparan_terpapar) {
        this.terparan_terpapar = terparan_terpapar;
    }

    public boolean isLain_lain() {
        return lain_lain;
    }

    public void setLain_lain(boolean lain_lain) {
        this.lain_lain = lain_lain;
    }

    public String getIsi_lain_lain() {
        return isi_lain_lain;
    }

    public void setIsi_lain_lain(String isi_lain_lain) {
        this.isi_lain_lain = isi_lain_lain;
    }
}
