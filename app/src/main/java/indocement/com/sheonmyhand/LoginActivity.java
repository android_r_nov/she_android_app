package indocement.com.sheonmyhand;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import indocement.com.sheonmyhand.helper.Api;
import indocement.com.sheonmyhand.helper.Helper;
import indocement.com.sheonmyhand.model.User;

public class LoginActivity extends AppCompatActivity {

    Button buttonLogin;
    EditText editTextNIK, editTextPassword;
    Api api = new Api();
    AlertDialog dialog;

    TextView textViewLupaPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        buttonLogin = (Button) findViewById(R.id.button_login);
        editTextNIK = (EditText) findViewById(R.id.editTextNIK);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        textViewLupaPassword = (TextView) findViewById(R.id.textViewLupaPassword);
        textViewLupaPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LupaPasswordActivity.class));
            }
        });


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void createDialog(String judul, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }

    public void login() {
        String nik = editTextNIK.getText().toString();
        String password = editTextPassword.getText().toString();
        final ProgressDialog progressDialog = Helper.showLoading(this, "Harap tunggu ...");
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String response = api.login((String) params[0], (String) params[1]);
                Log.d("LOGIN", (String) params[0]);
                Log.d("LOGIN", (String) params[1]);
                return response;
            }

            @Override
            protected void onPostExecute(Object o) {
                Log.d("LOGIN", o.toString());
                super.onPostExecute(o);
                progressDialog.dismiss();
                try {
                    JSONObject object = new JSONObject(o.toString());
                    if (object.getString("status").equals("Login berhasil")) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        User user = new User();
                        JSONObject object1 = object.getJSONObject("data");
                        user.setNama(object1.getString("nama"));
                        user.setNik(object1.getString("username"));
                        intent.putExtra("user", user);
                        startActivity(intent);
                        finish();
                    } else {
                        createDialog("Login gagal", "Mungkin NIK atau password Anda salah.\nSilahkan ulangi lagi");
                        dialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    createDialog("Login gagal", e.getMessage());
                    dialog.show();
                }

            }
        }.execute(new String[]{nik, password});


    }



}
