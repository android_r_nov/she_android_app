package indocement.com.sheonmyhand;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import indocement.com.sheonmyhand.helper.Api;

public class LupaPasswordActivity extends AppCompatActivity {

    Button kirim;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hand_logo);

        email = (EditText) findViewById(R.id.editTextEmail);
        kirim = (Button) findViewById(R.id.button_kirim);

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Api api = new Api();
                final String email_ = email.getText().toString();
                AsyncTask task = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        return api.forgetPasssword(email_);
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        createDialog("Perhatian", "Sebuah email telah dikirim ke alamat email Anda. silahkan periksa untuk melihat informasi login Anda.", 1);
                        dialog.show();
                        super.onPostExecute(o);
                    }
                };


                if (!email_.equals("") || email_ != null) {
                    task.execute(new String[]{});
                } else {
                    createDialog("Perhatian", "Email tidak boleh kosong", 0);
                    dialog.show();
                }

            }
        });
    }

    AlertDialog dialog;

    public void createDialog(String judul, String pesan, final int taskID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (taskID == 1)
                    finish();
            }
        });
        dialog = builder.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.menu_home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
