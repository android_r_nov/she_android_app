package indocement.com.sheonmyhand;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import indocement.com.sheonmyhand.adapter.ListSheInfoAdapter;
import indocement.com.sheonmyhand.helper.Api;
import indocement.com.sheonmyhand.helper.Helper;
import indocement.com.sheonmyhand.model.SheInfo;
import indocement.com.sheonmyhand.model.User;

public class SHEActivity extends AppCompatActivity implements View.OnClickListener {

    //    Button buttonSheTalkDocs, buttonIshelterSchedule, buttonKnowledge, buttonSheWinner;
    ListView list_she_info;
    AlertDialog dialog;
    ListSheInfoAdapter adapter;
    ArrayList<SheInfo> sheInfos = new ArrayList<SheInfo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_she);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hand_logo);

//        buttonSheTalkDocs = (Button) findViewById(R.id.buttonSheTalkDocs);
//        buttonIshelterSchedule = (Button) findViewById(R.id.buttonIshelterSchedule);
//        buttonKnowledge = (Button) findViewById(R.id.buttonKnowledge);
//        buttonSheWinner = (Button) findViewById(R.id.buttonSheWinner);
//        buttonSheTalkDocs.setOnClickListener(this);
//        buttonIshelterSchedule.setOnClickListener(this);
//        buttonKnowledge.setOnClickListener(this);
//        buttonSheWinner.setOnClickListener(this);

        adapter = new ListSheInfoAdapter(getApplicationContext(), sheInfos);
        list_she_info = (ListView) findViewById(R.id.list_she_info);
        list_she_info.setAdapter(adapter);
        loadSheInfo();
    }

    public void createDialog(String judul, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }


    private void loadSheInfo() {
        final Api api = new Api();
        final ProgressDialog progressDialog = Helper.showLoading(this, "Harap tunggu ...");
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String response = api.getAvailableSheInfo();
                return response;
            }

            @Override
            protected void onPostExecute(Object o) {
                Log.d("LOGIN", o.toString());
                super.onPostExecute(o);
                progressDialog.dismiss();
                try {
                    JSONArray jsArray = new JSONArray(o.toString());
                    for (int i = 0; i < jsArray.length(); i++) {
                        JSONObject object1 = jsArray.getJSONObject(i);
                        SheInfo sheInfo = new SheInfo();
                        sheInfo.setId(object1.getString("id_info"));
                        sheInfo.setTitle(object1.getString("judul"));
                        sheInfo.setUrl("http://sheindocement.webmurah-bogor.com/upload/file/" + object1.getString("file"));
                        sheInfos.add(sheInfo);

                        adapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    createDialog("Terjadi Error", e.getMessage());
                    dialog.show();
                }

            }
        }.execute();


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.menu_home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
//        Intent intent = new Intent(getApplicationContext(), SheDetailActivity.class);
//        switch (v.getId()) {
//            case R.id.buttonSheTalkDocs:
//                intent.putExtra("id", "001");
//                startActivity(intent);
//                break;
//            case R.id.buttonIshelterSchedule:
//                intent.putExtra("id", "002");
//                startActivity(intent);
//                break;
//            case R.id.buttonKnowledge:
//                intent.putExtra("id", "003");
//                startActivity(intent);
//                break;
//            case R.id.buttonSheWinner:
//                intent.putExtra("id", "004");
//                startActivity(intent);
//                break;
//            default:
//                break;
//        }
    }
}
