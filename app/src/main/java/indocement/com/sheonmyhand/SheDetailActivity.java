package indocement.com.sheonmyhand;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import indocement.com.sheonmyhand.helper.Api;
import indocement.com.sheonmyhand.helper.Helper;

public class SheDetailActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {

    private final static int REQUEST_CODE = 42;
    public static final int PERMISSION_CODE = 42042;
    Button buttonRight, buttonLeft;

    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    String actionBarTitle;
    //    public static final String SAMPLE_FILE = "dasar_android.pdf";
    public static String NAMA_FILE = "SHE_Talk_Documentation.pdf";
    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    private String TAG = "PDF Page";
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_she_detail);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hand_logo);

        pdfView = (PDFView) findViewById(R.id.pdfViewShe);
        buttonRight = (Button) findViewById(R.id.buttonRight);
        buttonLeft = (Button) findViewById(R.id.buttonLeft);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveNextPage();
            }
        });

        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movePrevPage();
            }
        });

        Bundle b;
        if (getIntent().getExtras() != null) {
            b = getIntent().getExtras();
            String url = b.getString("url", "");
            actionBarTitle = b.getString("judul", "");
//            loadData(id);
            getEbookPath(url);
        }

    }

    private void moveNextPage() {
        pdfView.jumpTo(pageNumber + 1);
    }

    private void movePrevPage() {
        pdfView.jumpTo(pageNumber - 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.menu_home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());

        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", actionBarTitle, page + 1, pageCount));
    }

//    private void displayFromAsset(String assetFileName) {
//        pdfFileName = getResources().getString(R.string.LSR_HANDBOOK);
//
//        pdfView.fromAsset(SAMPLE_FILE)
//                .defaultPage(pageNumber)
//                .onPageChange(this)
//                .enableAnnotationRendering(true)
//                .onLoad(this)
//                .scrollHandle(new DefaultScrollHandle(this))
//                .load();
//    }

    private void getEbookPath(String url) {
        final Api api = new Api();
        final AsyncTask as = new AsyncTask() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showDialog(progress_bar_type);
            }

            @Override
            protected Object doInBackground(Object[] params) {
//                String response = api.getSheINfoDetail((String) params[0]);

                Log.d("URL", (String) params[0]);
//                try {
//                    JSONObject object = new JSONObject(response);
                String urlString = (String) params[0];
//                    http://sheindocement.webmurah-bogor.com/upload/file/lsr.pdf
                String[] n = urlString.split("\\/");
                String namaFile = n[n.length - 1];
                NAMA_FILE = namaFile;
//                    displayFromUri(urlString);
                int count;
                try {
                    URL url = new URL(urlString);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    // this will be useful so that you can show a tipical 0-100%
                    // progress bar
                    int lenghtOfFile = conection.getContentLength();
                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream(),
                            8192);
                    // Output stream

                    File path = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DOCUMENTS);
                    File file = new File(path, NAMA_FILE);
                    if (!file.exists()) {
                        // Make sure the Pictures directory exists.
                        path.mkdirs();
                        OutputStream output = new FileOutputStream(file);
                        byte data[] = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                            // writing data to file
                            output.write(data, 0, count);
                        }
                        // flushing output
                        output.flush();
                        // closing streams
                        output.close();
                        input.close();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Error: ", e.getMessage());
                }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


                return "";
            }

            @Override
            protected void onProgressUpdate(Object[] values) {
                super.onProgressUpdate(values);
                pDialog.setProgress(Integer.parseInt((String) values[0]));
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                dismissDialog(progress_bar_type);
                File path = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOCUMENTS);
                File pdfFile = new File(path, NAMA_FILE);
//                File pdfFile = new File(Environment
//                        .getExternalStorageDirectory().toString()
//                        + "/Android/data/indocement.com.sheonmyhand/files/" + NAMA_FILE);
                displayFromFile(pdfFile);
            }

        };
        as.execute(new String[]{url});
    }

    private void displayFromFile(File file) {
        pdfFileName = getResources().getString(R.string.LSR_HANDBOOK);
//        Uri uri = Uri.parse(path);
        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .enableSwipe(false)
                .swipeHorizontal(true)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    /**
     * Listener for response to user permission request
     *
     * @param requestCode  Check that permission request code matches
     * @param permissions  Permissions that requested
     * @param grantResults Whether permissions granted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchPicker();
            }
        }
    }

    void launchPicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            //alert user that file manager not working
            Toast.makeText(this, "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    /**
     * Showing Dialog
     */

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }


}
