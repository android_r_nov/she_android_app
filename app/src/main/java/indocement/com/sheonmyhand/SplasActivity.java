package indocement.com.sheonmyhand;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class SplasActivity extends AppCompatActivity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private static int SPLASH_TIME_OUT_EXIT = 3000;
//@drawable/kerja_selamat
    private boolean IS_EXIT = false;
    ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splas);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            IS_EXIT = bundle.getBoolean("IS_EXIT", false);
        }

        Log.d("exit", "" + IS_EXIT);


        if (IS_EXIT){
            imageView2.setImageDrawable(getResources().getDrawable(R.drawable.hand_logo_closing));
            exit();
        }
        else {
            imageView2.setImageDrawable(getResources().getDrawable(R.drawable.kerja_selamat));
            comin();
        }


    }

    private void exit() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, SPLASH_TIME_OUT_EXIT);
    }

    private void comin() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplasActivity.this, LoginActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
