package indocement.com.sheonmyhand;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import indocement.com.sheonmyhand.helper.Api;
import indocement.com.sheonmyhand.helper.FileUploader;
import indocement.com.sheonmyhand.helper.Helper;
import indocement.com.sheonmyhand.model.MHR;
import indocement.com.sheonmyhand.model.User;

public class MHRActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextAuditTeam, editTextArea,
            editTextTotalObserved, editTextTotalPeople,
            editTextJobdesc, editTextResponseible1, editTextResponsible2,
            editTextPeople1, editTextPeople2, editTextViolation1, editTextViolation2;

    TimePicker timePickerJam;

    TextView textViewPath;
    Button buttonAttachment, buttonSubmit, buttonAmbilLokasi;
    DatePicker datePicker;
    public static final int REQUEST_LOKASI = 100;
    public static final int REQUEST_KAMERA = 101;
    public static final int RESULT_LOAD_IMAGE = 103;
    //    Api api = new Api();
    User user;
    AlertDialog dialog;
    //    String[] paths = new String[1];
    File attachment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhr);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.hand_logo);
        initViews();
        Bundle b;
        if (getIntent().getExtras() != null) {
            b = getIntent().getExtras();
            user = (User) b.getSerializable("user");
        }

    }

    private void initViews() {
        editTextAuditTeam = (EditText) findViewById(R.id.editTextAuditTeam);
        editTextArea = (EditText) findViewById(R.id.editTextArea);
        editTextTotalObserved = (EditText) findViewById(R.id.editTextTotalObserved);
        editTextTotalPeople = (EditText) findViewById(R.id.editTextTotalPeople);
        editTextJobdesc = (EditText) findViewById(R.id.editTextJobDesc);
        editTextResponseible1 = (EditText) findViewById(R.id.editTextResponsible1);
        editTextResponsible2 = (EditText) findViewById(R.id.editTextResponsible2);
        editTextPeople1 = (EditText) findViewById(R.id.editTextPeople1);
        editTextPeople2 = (EditText) findViewById(R.id.editTextPeople2);
        editTextViolation1 = (EditText) findViewById(R.id.editTextViolation1);
        editTextViolation2 = (EditText) findViewById(R.id.editTextViolation2);
        datePicker = (DatePicker) findViewById(R.id.datePickerTgl);
        textViewPath = (TextView) findViewById(R.id.textViewPath);
        timePickerJam = (TimePicker) findViewById(R.id.timePickerJam);

        buttonAttachment = (Button) findViewById(R.id.buttonAttachment);
        buttonAttachment.setOnClickListener(this);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(this);
        buttonAmbilLokasi = (Button) findViewById(R.id.buttonAmbilLokasi);
        buttonAmbilLokasi.setOnClickListener(this);

    }


    private String format = "";

    public String showTime(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        StringBuilder strBuilder = new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format);
        return strBuilder.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            case R.id.menu_home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public MHR generateMHR() {
        MHR mhr = new MHR();
        String auditTeam = editTextAuditTeam.getText().toString();
        String area = editTextArea.getText().toString();
        String totalObsr = editTextTotalObserved.getText().toString();
        String totalPeople = editTextTotalPeople.getText().toString();
        String jobdescription = editTextJobdesc.getText().toString();
        String people1 = editTextPeople1.getText().toString();
        String people2 = editTextPeople2.getText().toString();
        String responsible1 = editTextResponseible1.getText().toString();
        String responsible2 = editTextResponsible2.getText().toString();
        String violation1 = editTextViolation1.getText().toString();
        String violation2 = editTextViolation2.getText().toString();

        if (auditTeam.equals("") || auditTeam == null)
            return null;
        if (area.equals("") || area == null)
            return null;
        if (totalObsr.equals("") || totalObsr == null)
            return null;
        if (totalPeople.equals("") || totalPeople == null)
            return null;
        if (jobdescription.equals("") || jobdescription == null)
            return null;
        if (people1.equals("") || people1 == null)
            return null;
        if (people2.equals("") || people2 == null)
            return null;
        if (responsible1.equals("") || responsible1 == null)
            return null;
        if (responsible2.equals("") || responsible2 == null)
            return null;
        if (violation1.equals("") || violation1 == null)
            return null;
        if (violation2.equals("") || violation2 == null)
            return null;

        mhr.setAuditTeam(auditTeam);
        mhr.setArea(area);
        mhr.setTotalObserved(totalObsr);
        mhr.setTotalPeople(totalPeople);
        mhr.setJobDescription(jobdescription);
        mhr.setPeople1(people1);
        mhr.setPeople2(people2);
        mhr.setResponsible1(responsible1);
        mhr.setResponsible2(responsible2);
        mhr.setViolations1(violation1);
        mhr.setViolations2(violation2);
//                String tanggal = datePicker.getDayOfMonth() + "-" + datePicker.getMonth() + "-" + datePicker.getYear();
        mhr.setTgl(getDateFromDatePicker(datePicker));
        mhr.setTglInput(new Date());
        mhr.setNikPenginput(user.getNik());
        mhr.setJam(showTime(timePickerJam.getCurrentHour(), timePickerJam.getCurrentMinute()));
        return mhr;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSubmit:


                if (attachment != null) {
                    submit(generateMHR(), attachment);
                } else {
                    createDialog("Perhatian", "Harap sertakan Attachment !");
                    dialog.show();
                }

                break;
            case R.id.buttonAttachment:
                createDialogSelectGalleryOrCamera("Attachment", "Pilih attachment dari kamera atau galeri");
                dialog.show();
                break;
            case R.id.buttonAmbilLokasi:
                startActivityForResult(new Intent(getApplicationContext(), MapsActivity.class), REQUEST_LOKASI);
                break;
            default:
                break;
        }

    }


    public static Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public void createDialog(String judul, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }

    public void createDialogSelectGalleryOrCamera(String judul, String pesan) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(judul);
        builder.setMessage(pesan);
        builder.setPositiveButton("Kamera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    openCamera();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Galeri", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openGallery();
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }

    private void submit(final MHR mhr, final File file) {
        Helper.hideSoftKeyboard(this);
        if (mhr == null) {
            createDialog("Perhatian!", "Harap isikan kolom-kolom yang kosong");
            dialog.show();
        } else {
            final ProgressDialog progressDialog = Helper.showLoading(this, "Harap tunggu ...");
            AsyncTask task = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    return uploadFile(mhr, file);
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    progressDialog.dismiss();
                    if (o.toString().equals("Data berhasil terkirim")) {
                        createDialog("Berhasil", o.toString());
                        dialog.show();
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                resetViews();
                            }
                        });
                    } else {
                        createDialog("Gagal mengirim data", o.toString());
                        dialog.show();
                    }
                    Log.d("MHR", o.toString());
                }
            };

            task.execute();
        }

    }

    private void resetViews() {
        Log.d("MHR", "reset views");
        editTextAuditTeam.setText("");
        editTextArea.setText("");
        editTextTotalObserved.setText("");
        editTextTotalPeople.setText("");
        editTextJobdesc.setText("");
        editTextResponseible1.setText("");
        editTextResponsible2.setText("");
        editTextPeople1.setText("");
        editTextPeople2.setText("");
        editTextViolation1.setText("");
        editTextViolation2.setText("");
        attachment = null;
    }


    private void dispatchTakePictureIntent(int REQUEST_IMAGE_CAPTURE_CODE) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                        "indocement.com.sheonmyhand.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_CODE);
            }
        }
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_KAMERA && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(getApplicationContext(), photo);
            attachment = new File(getRealPathFromURI(tempUri));
            textViewPath.setText("Attachment : " + attachment.getName());
        }
        if (requestCode == REQUEST_LOKASI && resultCode == RESULT_OK) {
            if (data != null) {
                String kordinat = data.getStringExtra("kordinat");
                editTextArea.setText(kordinat);
            }

        }


        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            attachment = new File(getRealPathFromURI(selectedImage));
            textViewPath.setText("Attachment : " + attachment.getName());
//            try {
//                bmp = getBitmapFromUri(selectedImage);
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//            image_view.setImageBitmap(bmp);
//
//            //to know about the selected image width and height
//            Toast.makeText(MainActivity.this, image_view.getDrawable().getIntrinsicWidth()+" & "+image_view.getDrawable().getIntrinsicHeight(), Toast.LENGTH_SHORT).show();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        Log.d("PATH", path);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String uploadFile(MHR mhr, File file) {
        String result = "";
        String charset = "UTF-8";

        Api api = new Api();
        String requestURL = api.saveMHR(mhr);

        Log.d("URL", requestURL);
        try {
            FileUploader multipart = new FileUploader(requestURL, charset);

            multipart.addHeaderField("User-Agent", "CodeJava");
            multipart.addHeaderField("Test-Header", "Header-Value");
            multipart.addFormField("description", "Cool Pictures");
            multipart.addFormField("keywords", "Java,upload,Spring");
            multipart.addFilePart("file", file);
            List<String> response = multipart.finish();
            System.out.println("SERVER REPLIED:");


            for (String line : response) {
                System.out.println(line);
                result = line;
            }
        } catch (IOException ex) {
            System.err.println(ex);
            result = "Error : " + ex;
        }

        return result;
    }

    private void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, REQUEST_KAMERA);
    }

    private void openGallery() {
        try {
            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULT_LOAD_IMAGE);
        } catch (Exception exp) {
            Log.i("Error", exp.toString());
        }
    }

}
