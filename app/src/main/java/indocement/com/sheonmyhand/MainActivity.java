package indocement.com.sheonmyhand;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import indocement.com.sheonmyhand.model.User;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonExit, isopCardButton, mhrButton, lsrHandbookButton, sheInfoButton, button_test_camera;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hand_logo);
//        getSupportActionBar().setIcon(R.drawable.logo_dummy);
        try {
            Integer titleId = (Integer) Class.forName("com.android.internal.R$id")
                    .getField("action_bar_title").get(null);
            TextView tvtitle = (TextView) getWindow().findViewById(titleId);
            tvtitle.setTypeface(Typeface.SERIF);
            // check for null and manipulate the title as see fit
        } catch (Exception e) {
            Log.e("MAIN", "Failed to obtain action bar title reference");
        }


        Bundle b;
        if (getIntent().getExtras() != null) {
            b = getIntent().getExtras();
            user = (User) b.getSerializable("user");
        }
        buttonExit = (Button) findViewById(R.id.button_exit);
        isopCardButton = (Button) findViewById(R.id.button_isop_card);
        mhrButton = (Button) findViewById(R.id.button_mhr);
        lsrHandbookButton = (Button) findViewById(R.id.button_lsr_handbook);
        sheInfoButton = (Button) findViewById(R.id.button_she_info);
        button_test_camera = (Button) findViewById(R.id.button_test_camera);
        button_test_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TestCameraActivity.class));
            }
        });

        buttonExit.setOnClickListener(this);
        isopCardButton.setOnClickListener(this);
        mhrButton.setOnClickListener(this);
        lsrHandbookButton.setOnClickListener(this);
        sheInfoButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_exit:
                Intent intent = new Intent(MainActivity.this, SplasActivity.class);
                intent.putExtra("IS_EXIT", true);
                startActivity(intent);
                finish();
                break;
            case R.id.button_isop_card:
                Intent intentIsop = new Intent(MainActivity.this, IsopCardActivity.class);
                intentIsop.putExtra("user", user);
                startActivity(intentIsop);
                break;
            case R.id.button_mhr:
                Intent intentMhr = new Intent(MainActivity.this, MHRActivity.class);
                intentMhr.putExtra("user", user);
                startActivity(intentMhr);
                break;
            case R.id.button_lsr_handbook:
                Intent intentLsrHandbook = new Intent(MainActivity.this, LsrHandbookActivity.class);
                startActivity(intentLsrHandbook);
                break;
            case R.id.button_she_info:
                Intent intentSHEInfo = new Intent(MainActivity.this, SHEActivity.class);
                startActivity(intentSHEInfo);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
